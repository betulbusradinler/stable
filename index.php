<!doctype html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>

<?php
require_once "connection.php";
function GetIP(){
    if (!empty($_SERVER['HTTP_CLIENT_IP'])){
        $ip=$_SERVER['HTTP_CLIENT_IP'];
    }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip=$_SERVER['REMOTE_ADDR'];
    }

    return $ip;
}

$ip = GetIP(); // fonksiyon ile gerçek ip adresini buluyoruz.
$access_key = "c077eaedd20a0d4c8beb3d3c05e3588d";

$ch = curl_init('http://api.ipstack.com/'.$ip.'?access_key='.$access_key.'');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$data = curl_exec($ch);
$curl_put = curl_getinfo($ch);

curl_close($ch);


if(!curl_errno($ch) and $curl_put["http_code"] == 200){
    $api_result = json_decode($data, true);

    //VT İŞLEMLERİ
    $sql = $db->prepare("INSERT INTO users_location (ip,ulke) VALUES (?,?)");
    $insert = $sql->execute([$api_result['ip'],'asdasdas']);
    if(!$insert){
        $hata = $sorgu->error_info();
        echo "MYSQL HATASI".$hata[2];
    }

}else{
    echo "Api çalışamadı.";
}
?>

<div class="container" style="mt-2">
    <div class="row justify-content-center">
       <div class="col">
           <div class="card" style="width: 18rem;">
               <img src="<?php echo $api_result['location']['country_flag']?>" class="card-img-top" alt="...">
               <div class="card-body">
                   <h5 class="card-title">Kullanıcı Bilgileri</h5>
               </div>
               <ul class="list-group list-group-flush">
                   <li class="list-group-item"><?php echo $api_result['ip']; ?></li>
                   <li class="list-group-item"><?php echo $api_result['country_name']; ?></li>
                   <li class="list-group-item"><?php echo $api_result['city']; ?></li>
               </ul>
           </div>
       </div>
    </div>

</div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
</body>
</html>